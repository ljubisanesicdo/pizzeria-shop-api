package com.gitlab.ljubisanesicdo.service;

import com.gitlab.ljubisanesicdo.entity.AssignedGrocery;
import com.gitlab.ljubisanesicdo.entity.Grocery;
import com.gitlab.ljubisanesicdo.entity.Pizza;
import com.gitlab.ljubisanesicdo.repository.PizzaRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Iterator;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class PizzaService {

    private final PizzaRepository pizzaRepository;

    private final GroceryService groceryService;

    @PersistenceContext
    EntityManager em;

    @Transactional
    public Pizza save(Pizza newPizza) {

        Pizza tempPizza = new Pizza();
        tempPizza.setName(newPizza.getName());
        tempPizza.setSize(newPizza.getSize());
        tempPizza.setPrice(newPizza.getPrice());
        tempPizza.setDescription(newPizza.getDescription());
        Pizza savedPizza = pizzaRepository.save(tempPizza);


        for (Iterator<AssignedGrocery> iterator = newPizza.getAssignedGroceries().iterator(); iterator.hasNext(); ) {

            AssignedGrocery assignedGrocery = iterator.next();

            assignedGrocery.setPizza_id(savedPizza.getId());

            Grocery grocery = groceryService.findByName(assignedGrocery.getGrocery().getName());

            assignedGrocery.setGrocery_id(grocery.getId());

            assignedGrocery.setGrocery(grocery);

            em.persist(assignedGrocery);

            savedPizza.getAssignedGroceries().add(assignedGrocery);
        }

        log.info("Saving {}", savedPizza);
        return pizzaRepository.save(savedPizza);

    }

    public List<Pizza> findAll() {

        log.info("Finding all");
        return pizzaRepository.findAll();

    }

    public Pizza findById(Long id) {

        if (!pizzaRepository.existsById(id)) {
            log.error("No pizza with id {} existing", id);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ("No pizza with ID: " + id + " exist"));
        }

        log.info("Finding one by id {}", id);
        return pizzaRepository.findById(id).get();
    }

    public void delete(Long id) {

        if (!pizzaRepository.existsById(id)) {
            log.error("No id {} for deleting", id);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ("No pizza with ID: " + id + " exist"));
        }

        log.info("Deleting pizza with id {}", id);
        pizzaRepository.deleteById(id);

    }

    public Pizza update(Pizza updatedPizza, Long id) {

        Pizza pizza = findById(id);

        pizza.setName(updatedPizza.getName());
        pizza.setSize(updatedPizza.getSize());
        pizza.setPrice(updatedPizza.getPrice());
        pizza.setDescription(updatedPizza.getDescription());

        log.info("Updating pizza with id {}, with {}", id, updatedPizza);
        return pizzaRepository.save(pizza);

    }
}
