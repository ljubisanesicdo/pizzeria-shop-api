package com.gitlab.ljubisanesicdo.service;

import com.gitlab.ljubisanesicdo.entity.Grocery;
import com.gitlab.ljubisanesicdo.repository.GroceryRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class GroceryService {

    private final GroceryRepository groceryRepository;

    public Grocery add(Grocery newGrocery) {

        log.info("Saving {}", newGrocery);
        return groceryRepository.save(newGrocery);
    }

    public List<Grocery> findAll() {

        log.info("Finding all");
        return groceryRepository.findAll();

    }

    public Grocery findById(Long id) {

        if (!groceryRepository.existsById(id)) {
            log.error("No grocery with id {} exist", id);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No grocery with id " + id + " exist");
        }

        log.info("Finding one by id {}", id);
        return groceryRepository.findById(id).get();
    }

    public void delete(Long id) {

        if (!groceryRepository.existsById(id)) {
            log.error("No id {} for deleting", id);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No grocery with id " + id + " exist");
        }

        log.info("Deleting grocery with id {}", id);
        groceryRepository.deleteById(id);
    }

    public Grocery update(Grocery updatedGrocery, Long id) {

        Grocery grocery = findById(id);

        grocery.setName(updatedGrocery.getName());
        grocery.setPrice(updatedGrocery.getPrice());
        grocery.setUnitsOnStock(updatedGrocery.getUnitsOnStock());

        log.info("Updating grocery with id {}, with {}", id, updatedGrocery);
        return groceryRepository.save(grocery);
    }

    public Grocery findByName(String name) {

        Grocery grocery = groceryRepository.findByName(name);

        if (!groceryRepository.existsById(grocery.getId())) {
            log.error("No grocery with name {} exist", name);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No grocery with name " + name + " exist");
        }

        log.info("Finding one by name {}", name);
        return grocery;
    }
}
