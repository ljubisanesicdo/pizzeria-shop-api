package com.gitlab.ljubisanesicdo.entity;

import lombok.Getter;

import java.io.Serializable;
import java.util.Objects;
@Getter
public class AssignedGroceryId implements Serializable {

    private Long pizza_id;

    private Long grocery_id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof AssignedGroceryId)) return false;

        AssignedGroceryId that = (AssignedGroceryId) o;
        return Objects.equals(getPizza_id(), that.getPizza_id()) &&
                Objects.equals(getGrocery_id(), that.getGrocery_id());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPizza_id(), getGrocery_id());
    }
}
