package com.gitlab.ljubisanesicdo.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;
import java.math.BigDecimal;

@Entity
@Data
public class Grocery {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Name is required")
    @Size(min = 3, max = 30, message = "Name must be min 3 and max 30 characters long")
    @Pattern(regexp = "[a-zA-Z ]*", message = "Name can contain only letters")
    private String name;

    @NotNull(message = "Price is required")
    @DecimalMin(value = "0.1", message = "Minimum price is 0.1")
    @Digits(integer = 3, fraction = 2)
    private BigDecimal price;

    @NotNull(message = "Units on Stock is required")
    @DecimalMin(value = "0.1", message = "Minimal amount is 0.1")
    @Digits(integer = 4, fraction = 3)
    private BigDecimal unitsOnStock;

}
