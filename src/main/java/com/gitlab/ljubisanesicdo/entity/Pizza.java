package com.gitlab.ljubisanesicdo.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Pizza {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Name is required")
    @Size(min = 3, max = 30, message = "Name must be min 3 and max 30 characters long")
    @Pattern(regexp = "[a-zA-Z ]*", message = "Name can contain only letters")
    private String name;

    @NotEmpty(message = "Size is required")
    @Size(min = 3, max = 20, message = "Size must be min 3 and max 20 characters long")
    @Pattern(regexp = "[a-zA-Z ]*", message = "Size can contain only letters")
    private String size;

    @NotNull(message = "Price is required")
    @DecimalMin(value = "0.1", message = "Minimum price is 0.1")
    @Digits(integer = 3, fraction = 2)
    private BigDecimal price;

    @NotEmpty(message = "Description is required")
    @Size(min = 3, max = 255, message = "Description must be min 3 and max 255 characters long")
    private String description;

    @OneToMany(mappedBy = "pizza_id", cascade = CascadeType.MERGE)
    private List<AssignedGrocery> assignedGroceries = new ArrayList<>();

}
