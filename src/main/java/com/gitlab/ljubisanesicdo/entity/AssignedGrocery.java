package com.gitlab.ljubisanesicdo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "assigned_groceries")
@IdClass(AssignedGroceryId.class)
@Setter
@Getter
@ToString
public class AssignedGrocery {

    @Id
    @JsonIgnore
    private Long pizza_id;

    @Id
    @JsonIgnore
    private Long grocery_id;

    @Digits(integer = 2, fraction = 3)
    private BigDecimal amountUsed;

    @ManyToOne
    @JoinColumn(name = "grocery_id", referencedColumnName = "id", updatable = false, insertable = false)
    private Grocery grocery;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof AssignedGrocery)) return false;

        AssignedGrocery that = (AssignedGrocery) o;

        return getPizza_id().equals(that.getPizza_id()) &&
                getGrocery_id().equals(that.getGrocery_id());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPizza_id(), getGrocery_id());
    }
}
