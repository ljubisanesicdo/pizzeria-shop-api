package com.gitlab.ljubisanesicdo.repository;

import com.gitlab.ljubisanesicdo.entity.Grocery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroceryRepository extends JpaRepository<Grocery, Long> {

    Grocery findByName(String name);
}
