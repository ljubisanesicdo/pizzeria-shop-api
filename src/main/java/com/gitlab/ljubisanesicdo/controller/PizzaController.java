package com.gitlab.ljubisanesicdo.controller;

import com.gitlab.ljubisanesicdo.entity.Pizza;
import com.gitlab.ljubisanesicdo.service.PizzaService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/pizzas")
@AllArgsConstructor
@Slf4j
public class PizzaController {

    private final PizzaService pizzaService;

    @PostMapping()
    public Pizza add(@Valid @RequestBody Pizza newPizza) {

        if (newPizza.getId() != null) {
            log.error("Id key on creating new not allowed");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID key not allowed");
        }

        log.info("Sending request to Service for saving {}", newPizza);
        return pizzaService.save(newPizza);
    }

    @GetMapping()
    public List<Pizza> all() {

        log.info("Sending request to Service for finding all");
        return pizzaService.findAll();

    }

    @GetMapping("/{id}")
    public Pizza one(@PathVariable Long id) {

        log.info("Sending request to Service to find one by id {}", id);
        return pizzaService.findById(id);

    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {

        log.info("Sending request to Service for deleting by id {}", id);
        pizzaService.delete(id);

    }

    @PutMapping("/{id}")
    public Pizza update(@Valid @RequestBody Pizza updatedPizza, @PathVariable Long id) {

        if (updatedPizza.getId() != null) {
            log.error("Id key on updating not allowed");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID key not allowed");
        }

        log.info("Sending request to Service for updating {} by id {}", updatedPizza, id);
        return pizzaService.update(updatedPizza, id);
    }

}
