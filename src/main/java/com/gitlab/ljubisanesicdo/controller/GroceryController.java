package com.gitlab.ljubisanesicdo.controller;

import com.gitlab.ljubisanesicdo.entity.Grocery;
import com.gitlab.ljubisanesicdo.service.GroceryService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/groceries")
@AllArgsConstructor
@Slf4j
public class GroceryController {

    private final GroceryService groceryService;

    @PostMapping()
    public Grocery add(@Valid @RequestBody Grocery newGrocery) {

        if (newGrocery.getId() != null) {
            log.error("ID key on creating new not allowed");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID key not allowed");
        }

        log.info("Sending request to Service for saving {}", newGrocery);
        return groceryService.add(newGrocery);
    }

    @GetMapping()
    public List<Grocery> all() {

        log.info("Sending request to Service for finding all");
        return groceryService.findAll();
    }

    @GetMapping("/{id}")
    public Grocery one(@PathVariable Long id) {

        log.info("Sending request to Service for finding one by id {}", id);
        return groceryService.findById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {

        log.info("Sending request to Service for deleting grocery by id {}", id);
        groceryService.delete(id);
    }

    @PutMapping("/{id}")
    public Grocery update(@Valid @RequestBody Grocery updatedGrocery, @PathVariable Long id) {

        if (updatedGrocery.getId() != null) {
            log.error("ID key on updating not allowed");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID key not allowed");
        }

        log.info("Sending request to Service for updating {} by id {}", updatedGrocery, id);
        return groceryService.update(updatedGrocery, id);
    }

}
