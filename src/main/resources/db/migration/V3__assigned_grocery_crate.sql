CREATE TABLE `pizzeria`.`assigned_groceries`
(
    `pizza_id`    BIGINT        NOT NULL,
    `grocery_id`  BIGINT        NOT NULL,
    `amount_used` DECIMAL(5, 3) NOT NULL,
    PRIMARY KEY (`pizza_id`, `grocery_id`),
    INDEX `grocery_id_fk_idx` (`grocery_id` ASC) INVISIBLE,
    CONSTRAINT `pizza_id_fk`
        FOREIGN KEY (`pizza_id`)
            REFERENCES `pizzeria`.`pizza` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT `grocery_id_fk`
        FOREIGN KEY (`grocery_id`)
            REFERENCES `pizzeria`.`grocery` (`id`)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
)
    COMMENT = '		';
