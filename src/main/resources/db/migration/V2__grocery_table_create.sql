DROP TABLE IF EXISTS `grocery`;
CREATE TABLE `grocery`
(
    `id`          bigint(20) AUTO_INCREMENT,
    `name`        varchar(30)  NOT NULL,
    `price`       decimal(6, 2) NOT NULL,
    `units_on_stock`       decimal(7, 3) NOT NULL,

    PRIMARY KEY (`id`)
);
