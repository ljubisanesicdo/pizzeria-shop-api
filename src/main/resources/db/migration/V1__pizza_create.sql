DROP TABLE IF EXISTS `pizza`;
CREATE TABLE `pizza`
(
    `id`          bigint(20) AUTO_INCREMENT,
    `name`        varchar(30)  NOT NULL,
    `size`        varchar(20)  NOT NULL,
    `price`       decimal(6, 2) NOT NULL,
    `description` varchar(255)  NOT NULL,
    PRIMARY KEY (`id`)
);
