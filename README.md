# PIZZERIA SHOP API

## About

This is demo project for practicing backend API Java project.
The idea is to build basic web shop application.
 
Application is build using [Gradle](https://gradle.org/).
Implementing **Spring Boot Web, Spring Data JPA, Spring Data REST, Spring Data Validation.**
Database is [MySQL](https://www.mysql.com/) Database Service.

### Location

You can find this application on my [GitLab](https://gitlab.com/ljubisanesicdo) account 
* https://gitlab.com/ljubisanesicdo/pizzeria-shop-api

There is also matching UI - frontend [Angular](https://angular.io/) application
* https://gitlab.com/ljubisanesicdo/pizzeria-shop-ui

### Goal

Main goal at the beginning is to run basic CRUD operations on two binned entities and store the changes to data base.
Future plans is to run basic cash register logic, to add tests, implement user registration and online shopping.

## Install & Run

* Install Java 8
* Download and install gradle from [https://gradle.org/gradle-download](https://gradle.org/gradle-download/)
* ```git clone https://github.com/ljubisanesicdo/pizzeria-shop-api```
* Change directory to "pizzeria-shop-api" and run command "gradle bootRun".
* Once gradle build has finished and is running, go to [http://localhost:8080](http://localhost:8080) and fetch raw data
* To terminate gradle bootRun, kill process with ```CTRL + C```. 

## Configuration

### Configuration files

Folder **src/main/resources/** contains config files for **pizzeria-shop** Spring Boot application.
* **src/main/resources/application.properties** - main configuration file,
 here it is possible to change admin username/password as well as change the port number


### API Documentation

Full API documentation is done by implementing [Swagger](https://swagger.io/) professional toolset.
After you start application go to the web browser and visit 
        
        http://localhost:8080/swager-ui/
where you can find complete **up-to-date** graphical APIs that helps you to visualize
application APIs and provide you the ability to
**interact** with them.

## Database

For storage of data application use MySQL relation database management system.
All table creation, migration and initial data entry is done by implementing
[Flyway](https://flywaydb.org/) database Version Control.

Folder **src/main/resources/** contains and database version control files:
* **src/main/resources/db.migration/** - folder contains .sql files for each version

Files are named with capital ```V``` then double underscore ```__``` then name of particular action e.g. ```V1__pizza_table_create.sql``` 


## License

[MIT](https://choosealicense.com/licenses/mit/)

## Contact
Feel free to contact me with your questions at 
    
    ljubisanesicdo@gmail.com
    
    
 
